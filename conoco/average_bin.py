import pandas as pd 
import numpy as np

def average():
    pass


data = pd.read_csv('equip_failures_training_set.csv', na_values=['na'], dtype=np.float64)
print(data.shape)
cols = []

# average histogram values
for col in data.columns:
    if 'histogram' not in col:
        cols.append(col)

histograms = data.drop(columns=cols, axis=1)

histograms['sensor7_measurement'] = histograms[['sensor7_histogram_bin0', 
                                    'sensor7_histogram_bin1', 
                                    'sensor7_histogram_bin2', 
                                    'sensor7_histogram_bin3', 
                                    'sensor7_histogram_bin4', 
                                    'sensor7_histogram_bin5',
                                    'sensor7_histogram_bin6',
                                    'sensor7_histogram_bin7',
                                    'sensor7_histogram_bin8',
                                    'sensor7_histogram_bin9']].mean(axis=1)

histograms['sensor24_measurement'] = histograms[['sensor24_histogram_bin0', 
                                    'sensor24_histogram_bin1', 
                                    'sensor24_histogram_bin2', 
                                    'sensor24_histogram_bin3', 
                                    'sensor24_histogram_bin4', 
                                    'sensor24_histogram_bin5',
                                    'sensor24_histogram_bin6',
                                    'sensor24_histogram_bin7',
                                    'sensor24_histogram_bin8',
                                    'sensor24_histogram_bin9']].mean(axis=1)

histograms['sensor25_measurement'] = histograms[['sensor25_histogram_bin0', 
                                    'sensor25_histogram_bin1', 
                                    'sensor25_histogram_bin2', 
                                    'sensor25_histogram_bin3', 
                                    'sensor25_histogram_bin4', 
                                    'sensor25_histogram_bin5',
                                    'sensor25_histogram_bin6',
                                    'sensor25_histogram_bin7',
                                    'sensor25_histogram_bin8',
                                    'sensor25_histogram_bin9']].mean(axis=1)

histograms['sensor26_measurement'] = histograms[['sensor26_histogram_bin0', 
                                    'sensor26_histogram_bin1', 
                                    'sensor26_histogram_bin2', 
                                    'sensor26_histogram_bin3', 
                                    'sensor26_histogram_bin4', 
                                    'sensor26_histogram_bin5',
                                    'sensor26_histogram_bin6',
                                    'sensor26_histogram_bin7',
                                    'sensor26_histogram_bin8',
                                    'sensor26_histogram_bin9']].mean(axis=1)

histograms['sensor64_measurement'] = histograms[['sensor64_histogram_bin0', 
                                    'sensor64_histogram_bin1', 
                                    'sensor64_histogram_bin2', 
                                    'sensor64_histogram_bin3', 
                                    'sensor64_histogram_bin4', 
                                    'sensor64_histogram_bin5',
                                    'sensor64_histogram_bin6',
                                    'sensor64_histogram_bin7',
                                    ]].mean(axis=1)

histograms['sensor69_measurement'] = histograms[['sensor69_histogram_bin0', 
                                    'sensor69_histogram_bin1', 
                                    'sensor69_histogram_bin2', 
                                    'sensor69_histogram_bin3', 
                                    'sensor69_histogram_bin4', 
                                    'sensor69_histogram_bin5',
                                    'sensor69_histogram_bin6',
                                    'sensor69_histogram_bin7',
                                    'sensor69_histogram_bin8',
                                    'sensor69_histogram_bin9']].mean(axis=1)


histograms['sensor105_measurement'] = histograms[['sensor105_histogram_bin0', 
                                    'sensor105_histogram_bin1', 
                                    'sensor105_histogram_bin2', 
                                    'sensor105_histogram_bin3', 
                                    'sensor105_histogram_bin4', 
                                    'sensor105_histogram_bin5',
                                    'sensor105_histogram_bin6',
                                    'sensor105_histogram_bin7',
                                    'sensor105_histogram_bin8',
                                    'sensor105_histogram_bin9']].mean(axis=1)

print(histograms['sensor7_measurement'])
# average every 5 samples in the zero set
# groupby pandas
